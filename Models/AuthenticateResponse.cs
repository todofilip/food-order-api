﻿namespace FoodOrderApi.Models
{
    public class AuthenticateResponse
    {
        public string UserId { get; set; }

        public string Username { get; set; }

        public string Token { get; set; }

        public bool UseDarkTheme { get; set; }

        public AuthenticateResponse(User user, string token)
        {
            UserId = user.Id;
            Username = user.Username;
            Token = token;
            UseDarkTheme = user.UseDarkTheme;
        }
    }
}
