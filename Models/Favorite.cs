﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FoodOrderApi.Models
{
    public class Favorite
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string StoreId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        [BsonIgnore]
        public string StoreName { get; set; }

        [BsonIgnore]
        public string StoreImage { get; set; }

        [BsonIgnore]
        public string StoreCategory { get; set; }
    }
}
