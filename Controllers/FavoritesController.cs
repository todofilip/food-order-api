﻿using FoodOrderApi.Models;
using FoodOrderApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FoodOrderApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FavoritesController : ControllerBase
    {
        private readonly FavoriteService _favoriteService;

        public FavoritesController(FavoriteService favoriteService)
        {
            _favoriteService = favoriteService;
        }

        [HttpGet]
        public ActionResult<List<Favorite>> Get() =>
            _favoriteService.Get();

        [HttpGet("{id:length(24)}", Name = "GetFavorite")]
        public ActionResult<Favorite> Get(string id)
        {
            var favorite = _favoriteService.Get(id);

            if (favorite == null)
            {
                return NotFound();
            }

            return favorite;
        }

        [HttpGet("IsFavorite")]
        public ActionResult<bool> IsFavorite(string storeId, string userId)
        {
            var favorite = _favoriteService.Get(storeId, userId);
            return favorite != null;
        }

        [HttpGet("GetByUserId/{userId:length(24)}")]
        public ActionResult<List<Favorite>> GetByUserId(string userId) =>
            _favoriteService.GetByUserId(userId);

        [HttpPost]
        public ActionResult<Favorite> Create(Favorite favorite)
        {
            _favoriteService.Create(favorite);

            return CreatedAtRoute("GetFavorite", new { id = favorite.Id.ToString() }, favorite);
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var favorite = _favoriteService.Get(id);

            if (favorite == null)
            {
                return NotFound();
            }

            _favoriteService.Remove(favorite.Id);

            return NoContent();
        }
    }
}