﻿using FoodOrderApi.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace FoodOrderApi.Services
{
    public class FavoriteService
    {
        private readonly IMongoCollection<Favorite> _favorites;
        private readonly IMongoCollection<Store> _stores;

        public FavoriteService(IFoodOrderDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _favorites = database.GetCollection<Favorite>(settings.FavoritesCollectionName);
            _stores = database.GetCollection<Store>(settings.StoresCollectionName);
        }

        public List<Favorite> Get() =>
            _favorites.Find(favorite => true).ToList();

        public List<Favorite> GetByUserId(string userId)
        {
            var query = from f in _favorites.AsQueryable()
                        join s in _stores.AsQueryable() on f.StoreId equals s.Id
                        where f.UserId == userId
                        select new Favorite()
                        {
                            Id = f.Id,
                            StoreId = f.StoreId,
                            StoreName = s.Name,
                            StoreImage = s.Image,
                            StoreCategory = s.Category
                        };

            var orders = query.ToList();
            return orders;
        }

        public Favorite Get(string id) =>
            _favorites.Find<Favorite>(favorite => favorite.Id == id).FirstOrDefault();

        public Favorite Get(string storeId, string userId) =>
            _favorites.Find(favorite => favorite.StoreId == storeId && favorite.UserId == userId).FirstOrDefault();

        public Favorite Create(Favorite favorite)
        {
            _favorites.InsertOne(favorite);
            return favorite;
        }

        public void Update(string id, Favorite favoriteIn) =>
            _favorites.ReplaceOne(favorite => favorite.Id == id, favoriteIn);

        public void Remove(Favorite favoriteIn) =>
            _favorites.DeleteOne(favorite => favorite.Id == favoriteIn.Id);

        public void Remove(string id) =>
            _favorites.DeleteOne(favorite => favorite.Id == id);
    }
}