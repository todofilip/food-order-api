﻿namespace FoodOrderApi.Models
{
    public class FoodOrderDatabaseSettings : IFoodOrderDatabaseSettings
    {
        public string StoresCollectionName { get; set; }
        public string ProductsCollectionName { get; set; }
        public string UsersCollectionName { get; set; }
        public string OrdersCollectionName { get; set; }
        public string FavoritesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string Secret { get; set; }
    }

    public interface IFoodOrderDatabaseSettings
    {
        string StoresCollectionName { get; set; }
        string ProductsCollectionName { get; set; }
        string UsersCollectionName { get; set; }
        string OrdersCollectionName { get; set; }
        string FavoritesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string Secret { get; set; }
    }
}
