﻿using FoodOrderApi.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace FoodOrderApi.Services
{
    public class OrderService
    {
        private readonly IMongoCollection<Order> _orders;
        private readonly IMongoCollection<User> _users;
        private readonly IMongoCollection<Store> _stores;

        public OrderService(IFoodOrderDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _orders = database.GetCollection<Order>(settings.OrdersCollectionName);
            _users = database.GetCollection<User>(settings.UsersCollectionName);
            _stores = database.GetCollection<Store>(settings.StoresCollectionName);
        }

        public List<Order> Get()
        {
            var query = from o in _orders.AsQueryable()
                        join u in _users.AsQueryable() on o.UserId equals u.Id
                        join s in _stores.AsQueryable() on o.StoreId equals s.Id
                        orderby o.Date descending
                        select new Order()
                        {
                            Id = o.Id,
                            StoreId = o.StoreId,
                            Date = o.Date,
                            UserId = o.UserId,
                            Status = o.Status,
                            OrderLines = o.OrderLines,
                            Username = u.Username,
                            StoreName = s.Name
                        };

            var orders = query.ToList();
            return orders;
        }

        public Order Get(string id) =>
            _orders.Find<Order>(order => order.Id == id).FirstOrDefault();

        public List<Order> GetByUserId(string userId)
        {
            var query = from o in _orders.AsQueryable()
                        join s in _stores.AsQueryable() on o.StoreId equals s.Id
                        where o.UserId == userId
                        orderby o.Date descending
                        select new Order()
                        {
                            Id = o.Id,
                            StoreId = o.StoreId,
                            Date = o.Date,
                            UserId = o.UserId,
                            Status = o.Status,
                            OrderLines = o.OrderLines,
                            StoreName = s.Name
                        };

            var orders = query.ToList();
            return orders;
        }

        public Order Create(Order order)
        {
            _orders.InsertOne(order);
            return order;
        }

        public void Update(string id, Order orderIn) =>
            _orders.ReplaceOne(order => order.Id == id, orderIn);

        public void Remove(Order orderIn) =>
            _orders.DeleteOne(order => order.Id == orderIn.Id);

        public void Remove(string id) =>
            _orders.DeleteOne(order => order.Id == id);
    }
}