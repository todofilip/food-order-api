﻿using FoodOrderApi.Models;
using FoodOrderApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FoodOrderApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {
        private readonly StoreService _storeService;

        public StoresController(StoreService storeService)
        {
            _storeService = storeService;
        }

        [HttpGet("GetAll")]
        public ActionResult<List<Store>> GetAll(string category) =>
            _storeService.GetAll(category);

        [HttpGet("{id:length(24)}", Name = "GetStore")]
        public ActionResult<Store> Get(string id)
        {
            var store = _storeService.Get(id);

            if (store == null)
            {
                return NotFound();
            }

            return store;
        }

        [HttpPost]
        public ActionResult<Store> Create(Store store)
        {
            _storeService.Create(store);

            return CreatedAtRoute("GetStore", new { id = store.Id.ToString() }, store);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, Store storeIn)
        {
            var store = _storeService.Get(id);

            if (store == null)
            {
                return NotFound();
            }

            _storeService.Update(id, storeIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var store = _storeService.Get(id);

            if (store == null)
            {
                return NotFound();
            }

            _storeService.Remove(store.Id);

            return NoContent();
        }
    }
}