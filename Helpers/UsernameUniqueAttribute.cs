﻿using FoodOrderApi.Models;
using FoodOrderApi.Services;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FoodOrderApi.Helpers
{
    public class UsernameUniqueAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value, ValidationContext validationContext)
        {
            var userService = (UserService)validationContext.GetService(typeof(UserService));

            //todo: improve
            var model = userService.Get().Where(e => e.Username == value.ToString());

            if (model.Any() && model.FirstOrDefault().Id != ((User)validationContext.ObjectInstance).Id)
            {
                return new ValidationResult(GetErrorMessage(value.ToString()));
            }
            return ValidationResult.Success;
        }

        public string GetErrorMessage(string username)
        {
            return $"Username {username} is already in use.";
        }
    }
}
